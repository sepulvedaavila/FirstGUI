
#include <gtk/gtk.h>

G_MODULE_EXPORT void exit_button_clicked(GtkButton *button){
	gtk_main_quit();
}

int main(int argc, char *argv[]){
	GtkBuilder *gtkBuilder;
	GtkWidget *window;
	//GtkWidget *button;
	gtk_init(&argc, &argv);
	       
	gtkBuilder = gtk_builder_new();
	gtk_builder_add_from_file(gtkBuilder, "testGlade.glade", NULL);
	window = GTK_WIDGET(gtk_builder_get_object(gtkBuilder, "mywindow"));
	//button = GTK_ENTRY(gtk_builder_get_object(gtkBuilder, "exit_button"))
	gtk_builder_connect_signals(gtkBuilder, &window);

	g_object_unref(G_OBJECT(gtkBuilder));
	gtk_widget_show(window);
	gtk_main();
				        
	return 0;
}
